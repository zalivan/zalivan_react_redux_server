class AddImagesToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :images, :json
  end
end
