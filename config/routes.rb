Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace 'api' do
    namespace 'v1' do
      resources :articles do
      collection do
        get 'search'
      end
    end
    end
  end

  resources :users, only: [:create, :show, :update] do
    collection do
      post 'confirm'
      post 'login'
    end
  end

  # resources :articles
  # root 'api/v1/articles#index'
  # root 'http://360f7fc9.ngrok.io/users/'
  # root to: 'sessions#rooter'

  get 'results', to: 'results#index', as: 'results'

  post 'authenticate' => 'auth#authenticate'

  get '/auth/:provider/callback', to: 'sessions#create'

  delete '/logout', to: 'sessions#destroy'

end
