FactoryBot.define do
  sequence :email do |n|
    "user#{n}@test.com"
  end

  sequence :id do |n|
    n*5
  end

  factory :user do
    id
    username 'User'
    email
    password '12345678'
    password_confirmation '12345678'
  end
end
