FactoryBot.define do
  factory :article do
    title "MyString"
    body "MyText"
  end

  factory :invalid_article, class: 'Article' do
    title nil
    body nil
  end
end
