require 'spec_helper'

feature 'user sign in', %q{
  In order to be able to ask question
  As an user
  I want to be able to sign in
} do

  given(:user) { create(:user) }

  scenario 'Registered user try to sign in' do
    visit login_users_path
    byebug
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Log in'

    expect(page).to have_content 'Signed in successfully.'
    expect(current_path).to eq root_path
  end

  # scenario 'Non-registered user try to sign in' do
  #   visit users_path
  #   fill_in 'Email', with: 'wrong@test.com'
  #   fill_in 'Password', with: '12345678'
  #   click_on 'Log in'
  #
  #   expect(page).to have_content 'Invalid Email or password. Log in Email Password Remember me Sign up Forgot your password?'
  #   expect(current_path).to eq users_path
  # end

end