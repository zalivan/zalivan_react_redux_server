require 'spec_helper'

feature 'Create question', %Q{
  In order to get answer from community
  As an authenticated user
  I want to be able ask the questions
} do

  given(:user) { create(:user) }

  scenario 'Authenticated user creates question' do
    # @user = User.create!(email: 'user@test.com', password: '12345678')

    sign_in(user)

    visit questions_path
    click_on 'Ask question'
    fill_in 'Title', with: 'Test question'
    # save_and_open_page
    # byebug
    fill_in 'Body', with: 'test body'
    click_on 'Create'

    expect(page).to have_content "Your question successfully created."
    # expect(page).to have_content "Your question was not create!"
  end

  scenario 'Non-authenticated user ties to create question' do
    visit questions_path
    click_on 'Ask question'

    expect(page).to have_content 'You need to sign in or sign up before continuing.'
  end

end
