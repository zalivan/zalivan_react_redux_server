require 'spec_helper'

feature 'User answer', %q{
  In order to exchange my knowledge
  As an authenticated user
  I want o be able to create answer
} do

  given(:user) { create(:user) }
  given(:question) { create(:question) }

  scenario 'Authenticated user creates answer' do
    sign_in(user)
    visit question_path(question)

    fill_in 'Your answer', with: 'My answer'
    click_on 'Create'

    expect(current_path).to eg question_path(question)
    within '.answers' do
      expect(page).to have_content 'My answer'
    end
  end
end