require 'spec_helper'

RSpec.describe Article do
  it { should belong_to(:user) }

  it { should validate_presence_of :user }
  it { should validate_presence_of :title }
  it { should validate_presence_of :body }
  it { should validate_presence_of :tag }
end