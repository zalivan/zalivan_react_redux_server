require 'spec_helper'

RSpec.describe User do
  it { should have_many :articles }

  it { should validate_presence_of :email }
  it { should validate_presence_of :password }
  it { should validate_presence_of :username }
end