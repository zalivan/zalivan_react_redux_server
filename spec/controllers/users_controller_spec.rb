require 'spec_helper'

RSpec.describe UsersController, type: :controller do

  describe 'POST #login' do
    # let(:questions) { create_list(:question, 2) }
    let(:user) { create(:user) }

    before { get :login }

    it 'user tries to log in' do
      # expect(assigns(:questions)).to match_array(questions)
      expect { post :login, params: attributes_for(:user) }.to change(User, :count).by(1)
    end

    it 'renders index view' do
      expect(response).to render_template :index
    end
    end

  describe 'POST #create' do
    let(:user) { create(:user) }

    before { get :create }

    it 'user tries to sign up' do
      expect { post :create, params: attributes_for(:user) }.to change(User, :count).by(1)
    end

    it 'renders status Created' do
      post :create, params: attributes_for(:user)
      expect(response.message).to eq 'Created'
    end
  end

end