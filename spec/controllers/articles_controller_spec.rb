require 'rails_helper'

RSpec.describe Api::V1::ArticlesController, type: :controller do
  let(:article) { create(:article) }

  describe 'GET #index' do
    before do
      @user = create(:user)

    end
    let(:articles) { create_list(:article, 2) }

    before { get :index }

    it 'populates an array of all questions' do
      expect(assigns(:articles)).to match_array(articles)
    end

    it 'renders index view' do
      expect(response).to render_template :index
    end

  end



  # describe 'POST #create' do
  #   let(:question) { create(:question) }
  #   context 'with valid attributes' do
  #     it 'saves the new answer in the database' do
  #       expect { post :create, params: { answer: attributes_for(:answer), question_id: question } }.to change(question.answers, :count).by(1)
  #     end
  #     it 'redirect to question show view' do
  #       post :create, params: { answer: attributes_for(:answer), question_id: question }
  #       expect(response).to redirect_to question_path(assigns(:answer))
  #     end
  #   end
  #
  #   context 'with invalid attributes' do
  #     it 'does not save the answer' do
  #       expect { post :create, params: { answer: attributes_for(:invalid_answer), question_id: question } }.to_not change(Answer, :count)
  #     end
  #
  #     it 'redirect to question show view' do
  #       post :create, params: { answer: attributes_for(:invalid_answer), question_id: question }
  #       expect(response).to redirect_to question_path(assigns(:answer))
  #     end
  #   end
  # end
end
