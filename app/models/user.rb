class User < ApplicationRecord
  mount_uploader :image, ImageUploader
  has_secure_password
  has_many :articles

  def generate_auth_token
    payload = { user_id: self.id }
    AuthToken.encode(payload)
  end

  def self.find_or_create_from_auth_hash_google(auth_hash)
    user = User.where(uid: auth_hash[:googleId]).first_or_create
    user.update(
        email: auth_hash[:email],
        username: auth_hash[:name],
        profile_image: auth_hash[:imageUrl],
        image: auth_hash[:imageUrl],
        provider: 'google',
        uid: auth_hash[:googleId],
        password_digest: '123456'
    )
    user
  end

  def self.find_or_create_from_auth_hash(auth_hash)
    user = User.where(provider: auth_hash.provider, uid: auth_hash.uid).first_or_create
    user.update(
        email: auth_hash.info.email,
        username: auth_hash.info.nickname,
        profile_image: auth_hash.info.image,
        image: auth_hash.info.image,
        token: auth_hash.credentials.token,
        secret: auth_hash.credentials.secret,
        provider: auth_hash.provider,
        uid: auth_hash.uid,
        password_digest: '123456'
    )
    user
  end

  validates_presence_of :username
  validates_presence_of :email
  validates_uniqueness_of :email, case_sensitive: false
  validates_format_of :email, with: /@/
end
