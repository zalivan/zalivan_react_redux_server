class Article < ApplicationRecord
  mount_uploader :image, ImageUploader

  include PgSearch
  pg_search_scope :search_everywhere, against: [:title]

  belongs_to :user

  validates :user, presence: true
  validates :title, presence: true
  validates :body, presence: true
  validates :tag, presence: true
end
