class SessionsController < ApplicationController
  def create
    @user = User.find_or_create_from_auth_hash(auth_hash)
    @user.save
    auth_token = JsonWebToken.encode({user_id: @user.id, username: @user.username, profile_image: @user.profile_image, email: @user.email, provider: @user.provider, image: @user.image})

    redirect_to "http://febe4c82.ngrok.io/token=/#{auth_token}token=/"
  end

  def destroy
    if current_user
      session.delete(:user_id)
      flash[:success] = 'Sucessfully logged out!'
    end
    redirect_to root_path
  end

  def rooter
    redirect_to 'http://febe4c82.ngrok.io/users/'
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
