module Api
  module V1
    class ArticlesController < ApplicationController

      def index
        articles = Article.all.order(created_at: :desc)
        render json: {status: 'SUCCESS', message: 'Loaded articles', data: articles }, status: :ok
      end

      def search
        articles = Article.all
        search_articles = articles.select do |item|
          item.tag.downcase.include? params[:text].downcase or item.username.downcase.include? params[:text].downcase or item.title.downcase.include? params[:text].downcase or item.body.downcase.include? params[:text].downcase
        end
        render json: {status: 'SUCCESS', message: 'Loaded articles', data: search_articles }, status: :ok
      end

      def show
        user = User.find(params[:id]).articles.where(user_id: params[:id] )
        render json: {status: 'SUCCESS', message: 'Loaded user', data: user}, status: :ok
      end

      def create
        article = Article.new(article_params)

        if article.save
          render json: {status: 'SUCCESS', message: 'Saved article', data: article}, status: :ok
        else
          render json: {status: 'ERROR', message: 'Article not saved',
                        data: article.errors}, status: :unprocessable_entity
        end
      end

      def destroy
        article = Article.find(params[:id])
        article.destroy
        articles = Article.all
        render json: {status: 'SUCCESS', message: 'Deleted article', data: articles}, status: :ok
      end

      def update
        article = Article.find(params[:id])

        if article.update_attributes(article_params)
          render json: {status: 'SUCCESS', message: 'Updated article', data: article}, status: :ok
        else
          render json: {status: 'ERROR', message: 'Article not updates',
                        data: article.errors}, status: :unprocessable_entity
        end
      end

      private
      def article_params
        params.permit(:title, :body, :tag, :user_id, :username, :image)
      end
    end
  end
end