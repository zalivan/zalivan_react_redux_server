class UsersController < ApplicationController
  def create

    if params[:provider] === 'google'
      user = User.find_or_create_from_auth_hash_google(params[:result])

      if user.save
        auth_token = JsonWebToken.encode({user_id: user.id, username: user.username, profile_image: user.profile_image, email: user.email, image: user.image, provider: user.provider})
        render json: {status: 'User created successfully', data: auth_token}, status: :created
      else
        render json: { errors: user.errors.full_messages }, status: :bad_request
      end
    elsif User.where(email: params[:email]).first() != nil
      render json: {status: 'ERROR', message: "User didn't create", error: 'This user already exist' }, status: :unprocessable_entity
    else
      user = User.new(user_params)
      if user.save
        auth_token = JsonWebToken.encode({user_id: user.id, username: user.username, profile_image: user.profile_image, email: user.email, image: user.image, provider: user.provider})
        render json: {status: 'User created successfully', data: auth_token}, status: :created
      else
        render json: { errors: user.errors.full_messages }, status: :bad_request
      end
    end

  end

  def login
    user = User.find_by(email: params[:email].to_s.downcase)

    if user && user.authenticate(params[:password])
      auth_token = JsonWebToken.encode({user_id: user.id, username: user.username, email: user.email, profile_image: user.profile_image, image: user.image, provider: user.provider})
      render json: {auth_token: auth_token}, status: :ok
    else
      render json: {error: 'Invalid username / password'}, status: :unauthorized
    end
  end

  def show
    user = User.find(params[:id])
    render json: {status: 'SUCCESS', message: 'Loaded user', data: user}, status: :ok
  end

  def update
    user = User.find(params[:id])
    if user.update(user_params_for_update)
      auth_token = JsonWebToken.encode({user_id: user.id, username: user.username, email: user.email, profile_image: user.profile_image, image: user.image, provider: user.provider})
      render json: {status: 'SUCCESS', data: auth_token}, status: :ok
    else
      render json: {error: 'Problem with updating'}, status: :unauthorized
    end
  end

  private

  def user_params
    params.permit(:username, :email, :password, :password_confirmation, :image)
  end

  def user_params_for_update
    params.permit(:username, :email, :id, :image, :provider)
  end
end
